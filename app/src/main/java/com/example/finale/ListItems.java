package com.example.finale;


public class ListItems {
    private String Title;
    private String Description;
    private String DateCreated;
    private String estimatedFinishDate;
    private String estimatedFinishTime;
    private String category;
    private int layout;

    public ListItems(String Title, String Description,String DateCreated,String estimatedDate,String estimatedFinishTime,int layout,String category) {
        this.Title = Title;
        this.Description = Description;
        this.DateCreated = DateCreated;
        this.estimatedFinishDate = estimatedDate;
        this.estimatedFinishTime = estimatedFinishTime;
        this.layout = layout;
        this.category = category;
    }

    public ListItems(String Title,String Description,String DateCreated,String estimatedDate,int layout){
        this.Title = Title;
        this.Description = Description;
        this.DateCreated = DateCreated;
        this.estimatedFinishDate = estimatedDate;
        this.layout = layout;
    }
    public ListItems(String Title,String Description,String DateCreated){
        this.Title = Title;
        this.Description = Description;
        this.DateCreated = DateCreated;
    }

    public String getTitle() {
        return Title;
    }

    public String getDescription() {
        return Description;
    }

    public void setTitle(String title){
        Title = title;
    }

    public void setDescription(String description){
        Description = description;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public String getEstimatedFinishDate() {
        return estimatedFinishDate;
    }

    public void setEstimatedFinishDate(String estimatedFinishDate) {
        this.estimatedFinishDate = estimatedFinishDate;
    }

    public String getEstimatedFinishTime() {
        return estimatedFinishTime;
    }

    public void setEstimatedFinishTime(String estimatedFinishTime) {
        this.estimatedFinishTime = estimatedFinishTime;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEstimatedTime(){
        String date = "";
        String time = "";
        if(estimatedFinishDate != "" || estimatedFinishDate != null){
            date = estimatedFinishDate;
        }
        if(estimatedFinishTime != "" || estimatedFinishTime != null){
            time = estimatedFinishTime;
        }
        if(date != "" && time != ""){
            return "Estimated Finish by " + date + " " + time;
        }
        return "";
    }
}
