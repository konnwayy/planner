package com.example.finale;

import java.util.ArrayList;

public class ProjectItemsClass {

private ArrayList<String> steps;
private String ProjectTitle = "";
private String MaxBudget = "";
private String notes = "";
private String projectStartDate,projectEstimatedFinish;


    public ProjectItemsClass(){}

    public ProjectItemsClass(ArrayList<String>steps,String ProjectTitle,String notes,String MaxBudget ,String projectStartDate){
        this.steps = steps;
        this.ProjectTitle = ProjectTitle;
        this.notes = notes;
        this.MaxBudget = MaxBudget;
        this.projectStartDate = projectStartDate;
    }

    public ArrayList<String> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<String> steps) {
        this.steps = steps;
    }

    public String getProjectTitle() {
        return ProjectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        ProjectTitle = projectTitle;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(String projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public String getProjectEstimatedFinish() {
        return projectEstimatedFinish;
    }

    public void setProjectEstimatedFinish(String projectEstimatedFinish) {
        this.projectEstimatedFinish = projectEstimatedFinish;
    }

    public String getMaxBudget() {
        return MaxBudget;
    }

    public void setMaxBudget(String maxBudget) {
        MaxBudget = maxBudget;
    }
}
