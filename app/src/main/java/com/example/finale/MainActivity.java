package com.example.finale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{


    ArrayList<String> categories = new ArrayList<>();



    Tasks tasks = new Tasks();
    PersonalGoals personalGoals = new PersonalGoals();
    WorkGoals workGoals = new WorkGoals();
    CompletedGoals completedGoals = new CompletedGoals();
    InsperationalSiteClass siteClass = new InsperationalSiteClass();
    ProjectsClass projects = new ProjectsClass();

    public TextView toolbarTitle;
    public Button toolbarAddBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LoadCategories();
        if(categories.size() == 0){
            categories.add("Other");
            categories.add("Work");
        }
        SaveCategories();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbarTitle = (TextView)findViewById(R.id.toolbarTitle);
        toolbarAddBtn = (Button) findViewById(R.id.toolbarAddBtn);
        startUpFrag();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.tasks);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        if(id == R.id.add_category){
            Intent i = new Intent(getApplicationContext(),AddCategory.class);
            startActivityForResult(i,10);
            return true;
        }
        if(id == R.id.remove_category){
            Intent i = new Intent(getApplicationContext(),RemoveCategory.class);
            startActivity(i);
        }

        if(id == R.id.sort_categories){

        }

        if(id == R.id.insperational_site){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment12,
                   siteClass).commit();
        }

        return super.onOptionsItemSelected(item);
    }

    public void LoadCategories(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("CATEGORIES",null);
        Type type = new TypeToken<ArrayList<String>>(){}.getType();
        categories = gson.fromJson(json,type);

        if(categories == null){
            categories = new ArrayList<>();
        }
    }

    public void SaveCategories(){

            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("frag1",MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            Gson gson = new Gson();
            String json = gson.toJson(categories);
            editor.putString("CATEGORIES",json);
            editor.apply();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){//adds category
            if(requestCode == 10){
                Toast.makeText(getApplicationContext(),"Successfully added!",Toast.LENGTH_LONG).show();
                String newCategory = data.getStringExtra("CATEGORY");
                categories.add(newCategory);
                SaveCategories();
            }
        }
    }





    public void startUpFrag(){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment12,
                tasks).commit();
        toolbarTitle.setText("Tasks");
        toolbarAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tasks.openDialog();
            }
        });
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        toolbarAddBtn.setVisibility(View.VISIBLE);
        //if statement determines which menu item was selected
/*****************************************Tasks Menu Item Selected*********************************/

            if (id == R.id.tasks) {
                startUpFrag();

/*****************************************Personal Goals Menu Item Selected************************/

            } else if (id == R.id.personalGoals) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment12,
                        personalGoals).commit();
                toolbarTitle.setText("Personal Goals");
                toolbarAddBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        personalGoals.openDialog();
                    }
                });

/*****************************************Work Goals Menu Item Selected****************************/

            } else if (id == R.id.workGoals) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment12,
                        workGoals).commit();
                toolbarTitle.setText("Work Goals");
                toolbarAddBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        workGoals.openDialog();
                    }
                });

/*****************************************Completed Menu Item Selected*****************************/

            } else if (id == R.id.completed) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment12,
                        completedGoals).commit();
                toolbarAddBtn.setVisibility(View.INVISIBLE);
                toolbarTitle.setText("Completed");
            }

            else if(id == R.id.projects){
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment12,
                        projects).commit();
                toolbarTitle.setText("Projects");
                toolbarAddBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        projects.AddProject();
                    }
                });
            }



            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }


}
