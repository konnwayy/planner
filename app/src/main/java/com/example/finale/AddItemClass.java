package com.example.finale;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.TransactionTooLargeException;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AddItemClass extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, AdapterView.OnItemSelectedListener {

    public Button addBtn;
    public Button cancelBtn;
    public EditText title;
    public EditText description;
    public Switch setTimeSwitch;
    public Button setTimeBtn,setDateBtn,setColorBtn;
    public TextView setTimeTitle;
    public TextView dateTextView,timeTextView;
    public LinearLayout setTime,setDate;
    public String DateString = "";
    public String TimeString = "";
    public int layout = R.drawable.list_shape;

    Spinner spinner;


    String titleText,descriptionText,titleTV,descriptionTV;
    ArrayList<String> categories = new ArrayList<>();

    DialogFragment frag = new DatePickerFragment();
    DialogFragment timeFrag = new TimeSelecter();
    String category;


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        category = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_item_layout);
        LoadCategories();
        spinner = (Spinner)findViewById(R.id.categorySpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        addBtn = (Button)findViewById(R.id.finalAddBtn);
        cancelBtn = (Button)findViewById(R.id.backArrow);
        title = (EditText)findViewById(R.id.titleET);
        description = (EditText)findViewById(R.id.descriptionET);
        setTimeSwitch = (Switch)findViewById(R.id.setTimeSwitch);
        setTimeBtn = (Button)findViewById(R.id.setTimeBtn);
        setDateBtn = (Button)findViewById(R.id.setDateBtn);
        setTimeTitle = (TextView)findViewById(R.id.setTimeTitle);
        dateTextView = (TextView)findViewById(R.id.dateTextView);
        timeTextView = (TextView)findViewById(R.id.timeTextView);
        setTime = (LinearLayout)findViewById(R.id.setTimeLayout);
        setDate = (LinearLayout)findViewById(R.id.setDateLayout);
        setColorBtn = (Button)findViewById(R.id.selectColorBtn);


        setTimeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    setTime.setVisibility(View.VISIBLE);
                    setDate.setVisibility(View.VISIBLE);
                    setTimeTitle.setVisibility(View.VISIBLE);
                }
                if(!isChecked){
                    setTime.setVisibility(View.GONE);
                    setDate.setVisibility(View.GONE);
                    setTimeTitle.setVisibility(View.GONE);
                }
            }
        });

        setColorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SelectColorClass.class);
                startActivityForResult(i,1);
            }
        });


        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleText = title.getText().toString();
                if(TextUtils.isEmpty(titleText)){
                    title.setError("Title Cannot be Blank");
                }else{
                    descriptionText = description.getText().toString();
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("Title", titleText);
                    resultIntent.putExtra("Description",descriptionText);
                    if(DateString != null || DateString != "")
                        resultIntent.putExtra("EstimatedDate",DateString);
                    if(TimeString != null || TimeString != "")
                        resultIntent.putExtra("EstimatedTime",TimeString);
                    if(category.length() > 0)
                        resultIntent.putExtra("Category",category);
                    else
                        resultIntent.putExtra("Category","Other");
                    resultIntent.putExtra("layout",layout);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED,resultIntent);
                finish();
            }
        });

        setDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag.show(getSupportFragmentManager(),"datePicker");
            }
        });

        setTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timeFrag.show(getSupportFragmentManager(),"Delivery Time");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                String color = data.getStringExtra("color");

                switch (color){
                    case "Red":layout = R.drawable.list_shape_red;

                        break;
                    case "Blue":layout = R.drawable.list_shape_blue;
                        break;
                    case"Orange":layout = R.drawable.list_shape_orange;
                        break;
                    case"Green":layout = R.drawable.list_shape_green;
                        break;
                    case"Dark":layout = R.drawable.list_shape_dark;
                        break;
                    default:layout = R.drawable.list_shape;
                }
                setColorBtn.setBackgroundResource(layout);
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);
        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        DateString = DateFormat.getDateInstance().format(c.getTime());
        dateTextView.setText(DateString);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        int hour;
        String ampm;
        if(hourOfDay >= 13 ){
            ampm = "PM";
            hour = hourOfDay - 12;
        }else{
            ampm = "AM";
            hour = hourOfDay;
        }


        TimeString = hour + ":" + minute + " " + ampm;
        timeTextView.setText(TimeString);
    }


    public void LoadCategories(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("CATEGORIES",null);
        Type type = new TypeToken<ArrayList<String>>(){}.getType();
        categories = gson.fromJson(json,type);

        if(categories == null){
            categories = new ArrayList<>();
        }
    }
}
