package com.example.finale;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{
    public ArrayList<ListItems> mList;
    private OnItemClickListener mListener;
    private int position;


    public interface OnItemClickListener {
        void onItemClick(int position);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mTextView1;
        public TextView mTextView2;
        public TextView dateCreated;
        public TextView estimatedEndBy;
        public LinearLayout layout;
        public TextView category;
        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mTextView1 = itemView.findViewById(R.id.titleTV);
            mTextView2 = itemView.findViewById(R.id.description);
            dateCreated = itemView.findViewById(R.id.dateCreated);
            estimatedEndBy = itemView.findViewById(R.id.estimatedEndBy);
            layout = itemView.findViewById(R.id.listItemLayout);
            category = itemView.findViewById(R.id.list_item_category);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }



    public RecyclerAdapter(ArrayList<ListItems> List){
        mList = List;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_layout,viewGroup,false);
        ViewHolder vh =  new ViewHolder(v,mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ListItems currentItem = mList.get((getItemCount() - 1) - i);
        viewHolder.mTextView1.setText(currentItem.getTitle());
        viewHolder.mTextView2.setText(currentItem.getDescription());
        viewHolder.dateCreated.setText((currentItem.getDateCreated()));
        if(!currentItem.getEstimatedTime().equals("Estimated Finish by  "))
            viewHolder.estimatedEndBy.setText((currentItem.getEstimatedTime()));
        else
            viewHolder.estimatedEndBy.setText("");
        viewHolder.layout.setBackgroundResource(currentItem.getLayout());
        viewHolder.category.setText(currentItem.getCategory());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


}


