package com.example.finale;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.ViewHolder>{

    public ArrayList<String> mList;
    private OnItemClickListener mListener;


    public interface OnItemClickListener {
        void onItemClick(int position);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView Step;
        public ViewHolder(@NonNull View itemView, final StepAdapter.OnItemClickListener listener) {
            super(itemView);
            Step = itemView.findViewById(R.id.stepTextView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }



    public StepAdapter(ArrayList<String> List){
        mList = List;
    }

    @NonNull
    @Override
    public StepAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {//sets layout of each item to projects_item_layout
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.steps_item_layout,viewGroup,false);
        ViewHolder vh =  new ViewHolder(v,mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull StepAdapter.ViewHolder viewHolder, int i) {
        String currentItem = mList.get(i);
        viewHolder.Step.setText(currentItem);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


}
