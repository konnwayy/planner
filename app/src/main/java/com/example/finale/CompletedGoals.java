package com.example.finale;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Collections;


import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class CompletedGoals extends Fragment {
    private RecyclerView mRecyclerView;
    private  RecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public View v;

    ArrayList<ListItems> Goals = new ArrayList<>();
    ListItems temp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.list_layout_items, container, false);
        loadData();
        mRecyclerView = (RecyclerView)v.findViewById(R.id.recyclerView1);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new RecyclerAdapter(Goals);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //changeItem(position);
                Toast.makeText(getActivity(),"You cannot Change an Accomplishment",Toast.LENGTH_SHORT).show();
            }
        });
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder dragged, RecyclerView.ViewHolder target) {
                int positionDragged = dragged.getAdapterPosition();
                int positionTarget = target.getAdapterPosition();

                Collections.swap(Goals,(Goals.size()-1) - positionDragged,(Goals.size()-1) - positionTarget);

                mAdapter.notifyItemMoved(positionDragged,positionTarget);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                temp = Goals.get((Goals.size() - 1) - viewHolder.getAdapterPosition());
                final int deletedPosition = viewHolder.getAdapterPosition();
                Goals.remove((Goals.size() - 1) - viewHolder.getAdapterPosition());
                saveData();
                mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                Toast.makeText(getContext(), "Task Deleted", Toast.LENGTH_SHORT).show();

                Snackbar sb = Snackbar.make(getView(),"Are you Sure you Want to Delete",15000)
                        .setAction("UNDO", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Goals.add(deletedPosition,temp);

                                mAdapter.notifyItemInserted(deletedPosition);
                                saveData();
                            }
                        });
                sb.show();
            }
        }).attachToRecyclerView(mRecyclerView);

        return v;
    }


    public void insertItem(String title, String description,String dateCreated) {
        Goals.add(Goals.size(), new ListItems(title,description,dateCreated));
        mAdapter.notifyItemInserted(0);
        mRecyclerView.scrollToPosition(View.SCROLL_INDICATOR_TOP);
        mLayoutManager.scrollToPosition(0);
    }

    public void removeItem(int position) {
        Goals.remove((Goals.size() - 1) - position);
        mAdapter.notifyItemRemoved(position);
        saveData();
    }




//***** change selected item *********

    public void changeItem(int position) {
        int newPosition = (Goals.size() - 1) - position;
        String string1 = Goals.get(newPosition).getTitle();
        String string2 = Goals.get(newPosition).getDescription();
        Intent i = new Intent(getActivity(),ChangeItemClass.class);
        i.putExtra("goalPosition",position);
        i.putExtra("title",string1);
        i.putExtra("description",string2);
        startActivityForResult(i,2);
    }

    public void itemChangedSelected(int position,String title,String description){
        Goals.get((Goals.size() - 1) - position).setTitle(title);
        Goals.get((Goals.size() - 1) - position).setDescription(description);
        mAdapter.notifyItemChanged(position);
        saveData();
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {//item is added
            if (resultCode == RESULT_OK) {
                Calendar c = Calendar.getInstance();
                String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
                String dateCreated = "Created on - " + currentDate;
                String title = data.getStringExtra("Title");
                String description = data.getStringExtra("Description");
                insertItem(title,description,dateCreated);
                Toast.makeText(getContext(),"New Item Added",Toast.LENGTH_LONG).show();
                saveData();
            }
            if (resultCode == RESULT_CANCELED) {

            }
        }
        if(requestCode == 2){//item was clicked and is being changed or removed
            String type = "";
            if(data != null){
                type = data.getStringExtra("type");
                if(resultCode == RESULT_OK){
                    if(type.equals("CHANGE")){
                        String title = data.getStringExtra("Title");
                        String description = data.getStringExtra("Description");
                        int itemPosition = data.getIntExtra("changePosition",0);
                        itemChangedSelected(itemPosition,title,description);
                        Toast.makeText(getActivity(),"change",Toast.LENGTH_SHORT).show();
                    }
                    if(type.equals("REMOVE")){
                        boolean removeItem = data.getBooleanExtra("remove",false);
                        int removePosition = data.getIntExtra("removePosition",0);
                        removeItem(removePosition);
                        Toast.makeText(getActivity(),"remove",Toast.LENGTH_SHORT).show();
                    }
                }
            }


        }
    }

    public void saveData(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("frag1",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(Goals);
        editor.putString("completed",json);
        editor.apply();
    }

    public void loadData(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("completed",null);
        Type type = new TypeToken<ArrayList<ListItems>>(){}.getType();
        Goals = gson.fromJson(json,type);

        if(Goals == null){
            Goals = new ArrayList<>();
        }
    }


}