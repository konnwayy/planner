package com.example.finale;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import java.util.ArrayList;

public class AddProjectClass extends AppCompatActivity {

    EditText Title,Description,Step,MaxBudget;
    Button addStep,addProjectBtn,backArrow;
    FrameLayout stepsView;
    ArrayList<String> Steps = new ArrayList<>();

    private RecyclerView RecyclerView;
    private  StepAdapter Adapter;
    private RecyclerView.LayoutManager LayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.projects_add_modify_item_layout);

        RecyclerView = (RecyclerView)findViewById(R.id.ProjectsRecyclerView);
        RecyclerView.setHasFixedSize(true);
        LayoutManager = new LinearLayoutManager(getApplicationContext());
        Adapter = new StepAdapter(Steps);
        RecyclerView.setLayoutManager(LayoutManager);
        RecyclerView.setAdapter(Adapter);

        stepsView = (FrameLayout)findViewById(R.id.stepsLayout);
        if(Steps.size() <= 0)
            stepsView.setVisibility(View.GONE);
        else
            stepsView.setVisibility(View.VISIBLE);
        Title = (EditText)findViewById(R.id.projectTitle);
        Description = (EditText)findViewById(R.id.projectNotes);
        Step = (EditText)findViewById(R.id.ProjectStep);
        MaxBudget = (EditText)findViewById(R.id.projectBudget);
        addStep = (Button)findViewById(R.id.addStepProject);
        addProjectBtn = (Button)findViewById(R.id.toolbarAddProjectBtn);
        backArrow = (Button)findViewById(R.id.backArrow);


        addProjectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = Title.getText().toString();
                if(TextUtils.isEmpty(text))
                    Title.setError("This cannot be blank");
                else{
                    Intent result = new Intent();
                    result.putExtra("Title",Title.getText().toString());
                    result.putExtra("Description",Description.getText().toString());
                    result.putStringArrayListExtra("Steps",Steps);
                    result.putExtra("MaxBudget",MaxBudget.getText().toString());
                    setResult(1,result);
                    finish();
                }
            }
        });

        addStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Steps.add(Step.getText().toString());
                Step.setText("");
                Adapter.notifyItemChanged(0);
                stepsView.setVisibility(View.VISIBLE);
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });


    }
}
