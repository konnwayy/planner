package com.example.finale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;


public class SelectColorClass extends AppCompatActivity {

    RadioButton red,blue,orange,green,dark;
    Button select;

    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_item_color);

        red = (RadioButton)findViewById(R.id.red);
        blue = (RadioButton)findViewById(R.id.blue);
        orange = (RadioButton)findViewById(R.id.orange);
        green = (RadioButton)findViewById(R.id.green);
        dark = (RadioButton)findViewById(R.id.grey);
        select = (Button)findViewById(R.id.selectBtn);

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        i = new Intent();

        red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i.putExtra("color","Red");
                setResult(RESULT_OK,i);
            }
        });

        blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i.putExtra("color","Blue");
                setResult(RESULT_OK,i);
            }
        });

        orange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i.putExtra("color","Orange");

                setResult(RESULT_OK,i);
            }
        });

        green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i.putExtra("color","Green");
                setResult(RESULT_OK,i);
            }
        });

        dark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i.putExtra("color","Dark");
                setResult(RESULT_OK,i);
            }
        });
    }
}
