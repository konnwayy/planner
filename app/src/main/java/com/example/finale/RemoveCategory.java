package com.example.finale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class RemoveCategory extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    ArrayList<String> categories = new ArrayList<>();
    int selectedPosition;
    Spinner spinner;

    Button remove,cancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remove_category);

        remove = (Button)findViewById(R.id.removeCategoryBtn);
        cancel = (Button)findViewById(R.id.categoryCancelBtn);

        LoadCategories();
        spinner = (Spinner)findViewById(R.id.removeSpinner);
        loadSpinner();


        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedPosition != -1){
                    categories.remove(selectedPosition);
                    loadSpinner();
                    SaveCategories();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    public void loadSpinner(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedPosition = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        selectedPosition = -1;
    }


    public void LoadCategories(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("CATEGORIES",null);
        Type type = new TypeToken<ArrayList<String>>(){}.getType();
        categories = gson.fromJson(json,type);

        if(categories == null){
            categories = new ArrayList<>();
        }
    }

    public void SaveCategories(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("frag1",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(categories);
        editor.putString("CATEGORIES",json);
        editor.apply();
    }
}
