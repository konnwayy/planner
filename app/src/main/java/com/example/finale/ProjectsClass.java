package com.example.finale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class ProjectsClass extends Fragment {

    public View v;
    private RecyclerView mRecyclerView;
    private  ProjectsRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public ArrayList<ProjectItemsClass>Projects = new ArrayList<>();
    public ProjectItemsClass temp;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.projects_list_layout_items, container, false);

        LoadData();
        mRecyclerView = (RecyclerView)v.findViewById(R.id.projectsRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new ProjectsRecyclerAdapter(Projects);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ProjectsRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                int projectPosition = (Projects.size()-1)-position;
                ArrayList<String> selectedSteps = new ArrayList<>();
                selectedSteps = Projects.get(projectPosition).getSteps();
                String title = Projects.get(projectPosition).getProjectTitle();
                String notes = Projects.get(projectPosition).getNotes();
                String budget = Projects.get(projectPosition).getMaxBudget();
                Intent i = new Intent(getActivity(),ChangeProjectsClass.class);
                i.putExtra("STEPS",selectedSteps);
                i.putExtra("TITLE",title);
                i.putExtra("NOTES",notes);
                i.putExtra("BUDGET",budget);
                i.putExtra("POSITION",projectPosition);
                startActivityForResult(i,1);
            }
        });




        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder dragged, RecyclerView.ViewHolder target) {
                int positionDragged = dragged.getAdapterPosition();
                int positionTarget = target.getAdapterPosition();

                Collections.swap(Projects,(Projects.size()-1) - positionDragged,(Projects.size()-1) - positionTarget);

                mAdapter.notifyItemMoved(positionDragged,positionTarget);
                SaveData();
                return true;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                temp = Projects.get(viewHolder.getAdapterPosition());
                final int deletePosition = (viewHolder.getAdapterPosition());
                Projects.remove(viewHolder.getAdapterPosition());
                SaveData();
                mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                Toast.makeText(getContext(), "Project Deleted", Toast.LENGTH_SHORT).show();

                Snackbar sb = Snackbar.make(getView(),"Are you Sure you Want to Delete",15000)
                        .setAction("UNDO", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Projects.add(deletePosition,temp);

                                mAdapter.notifyItemInserted(deletePosition);
                                SaveData();
                            }
                        });
                sb.show();
            }
        }).attachToRecyclerView(mRecyclerView);



        return v;
    }



    public void AddProject(){
        Intent i = new Intent(getActivity(),AddProjectClass.class);
        startActivityForResult(i,0);
    }

    public void InsertItem(String Title,String Description,ArrayList<String>steps,String MaxBudget,String dateCreated){
        Projects.add(Projects.size(), new ProjectItemsClass(steps,Title,Description,MaxBudget,dateCreated));
        mAdapter.notifyItemInserted(0);
        mRecyclerView.scrollToPosition(View.SCROLL_INDICATOR_TOP);
        mLayoutManager.scrollToPosition(0);
        SaveData();
    }

    public void ChangeProject(String Title, String Description,ArrayList<String> steps,String MaxBudget,int position){
        Projects.get(position).setProjectTitle(Title);
        Projects.get(position).setNotes(Description);
        Projects.get(position).setSteps(steps);
        Projects.get(position).setMaxBudget(MaxBudget);
        mAdapter.notifyItemChanged(position);
        SaveData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String Title, Description,MaxBudget;
        ArrayList<String>Steps;
        if(requestCode == 0){//added project
            if(resultCode == 1){
                Calendar c = Calendar.getInstance();
                String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
                String dateCreated = "Created on - " + currentDate;
                Title = data.getStringExtra("Title");
                Description = data.getStringExtra("Description");
                Steps = data.getStringArrayListExtra("Steps");
                MaxBudget = data.getStringExtra("MaxBudget");
                InsertItem(Title,Description,Steps,MaxBudget,dateCreated);
            }
        }
        if(requestCode == 1){
            if(resultCode == 2){
                Title = data.getStringExtra("TITLE");
                Description = data.getStringExtra("DESCRIPTION");
                Steps = data.getStringArrayListExtra("STEPS");
                MaxBudget = data.getStringExtra("BUDGET");
                int position = data.getIntExtra("POSITION",-1);
                ChangeProject(Title,Description,Steps,MaxBudget,position);
            }
        }

    }

    public void SaveData(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("frag1",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(Projects);
        editor.putString("Projects",json);
        editor.apply();
    }

    public void LoadData(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Projects",null);
        Type type = new TypeToken<ArrayList<ProjectItemsClass>>(){}.getType();
        Projects = gson.fromJson(json,type);
        if(Projects == null){
            Projects = new ArrayList<>();
        }
    }
}
