package com.example.finale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ChangeItemClass extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, AdapterView.OnItemSelectedListener {

    Button changeBtn;
    Button BackArrowBtn;
    Button CompleteItemBtn;
    EditText title;
    EditText description;
    public Switch setTimeSwitch;
    public Button setTimeBtn,setDateBtn,setColorBtn;
    public TextView setTimeTitle;
    public TextView dateTextView,timeTextView;
    public LinearLayout setTime,setDate;
    public String DateString = "";
    public String TimeString = "";
    public String category;
    int categoryIndex;

    Intent resultIntent;
    int layout;

    String titleText,descriptionText,newCategory;

    Spinner spinner;

    int position;

    DialogFragment frag = new DatePickerFragment();
    DialogFragment timeFrag = new TimeSelecter();

    ArrayList<String> categories = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_item_layout);

        LoadCategories();
        spinner = (Spinner)findViewById(R.id.changedCategorySpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        Bundle args = this.getIntent().getExtras();
        String argsTitle = args.getString("title");
        String argsDescription = args.getString("description");
        String argsEstimatedDate = args.getString("estimatedDate");
        String argsEstimatedTime = args.getString("estimatedTime");
        category = args.getString("category");
        layout = args.getInt("layout");
        position = args.getInt("goalPosition");
        changeBtn = (Button)findViewById(R.id.changeItemBtn);
        BackArrowBtn = (Button)findViewById(R.id.backArrow);
        title = (EditText)findViewById(R.id.changeTitleET);
        description = (EditText)findViewById(R.id.changeDescriptionET);
        setTimeSwitch = (Switch)findViewById(R.id.setTimeSwitch);
        setTimeBtn = (Button)findViewById(R.id.setTimeBtn);
        setDateBtn = (Button)findViewById(R.id.setDateBtn);
        setTimeTitle = (TextView)findViewById(R.id.setTimeTitle);
        dateTextView = (TextView)findViewById(R.id.dateTextView);
        timeTextView = (TextView)findViewById(R.id.timeTextView);
        setTime = (LinearLayout)findViewById(R.id.setTimeLayout);
        setDate = (LinearLayout)findViewById(R.id.setDateLayout);
        setColorBtn = (Button)findViewById(R.id.selectColorBtn);
        setColorBtn.setBackgroundResource(layout);
        CompleteItemBtn = (Button)findViewById(R.id.completeItemBtn);

        title.setText(argsTitle);
        description.setText(argsDescription);
        dateTextView.setText(argsEstimatedDate);
        timeTextView.setText(argsEstimatedTime);
        for(int i = 0;i < categories.size();i++){
            if(category.equals(categories.get(i).toString())){
                categoryIndex = i;
            }
        }
        spinner.setSelection(categoryIndex);
        CompleteItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),CheckAction.class);
                startActivityForResult(i,6);
            }
        });

        changeBtn.setOnClickListener(new View.OnClickListener() {//changes item to whatever the user types in
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),CheckAction.class);
                startActivityForResult(i,5);
            }
        });

        BackArrowBtn.setOnClickListener(new View.OnClickListener() {//cancel btn
            @Override
            public void onClick(View v) {
                resultIntent = new Intent();
                setResult(RESULT_CANCELED,resultIntent);
                finish();
            }
        });



        setTimeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    setTime.setVisibility(View.VISIBLE);
                    setDate.setVisibility(View.VISIBLE);
                    setTimeTitle.setVisibility(View.VISIBLE);
                }
                if(!isChecked){
                    setTime.setVisibility(View.GONE);
                    setDate.setVisibility(View.GONE);
                    setTimeTitle.setVisibility(View.GONE);
                }
            }
        });

        setDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag.show(getSupportFragmentManager(),"datePicker");
            }
        });

        setTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeFrag.show(getSupportFragmentManager(),"Delivery Time");
            }
        });

        setColorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SelectColorClass.class);
                startActivityForResult(i,1);
            }
        });
      }


    public void changeItem(){
        titleText = title.getText().toString();
        descriptionText = description.getText().toString();
        resultIntent = new Intent();
        resultIntent.putExtra("Title", titleText);
        resultIntent.putExtra("Description",descriptionText);
        resultIntent.putExtra("changePosition",position);
        if(DateString != null || DateString != "")
            resultIntent.putExtra("EstimatedDate",DateString);
        if(TimeString != null || TimeString != "")
            resultIntent.putExtra("EstimatedTime",TimeString);
        resultIntent.putExtra("Category",newCategory);
        resultIntent.putExtra("layout",layout);
        resultIntent.putExtra("Completed",false);
        setResult(RESULT_OK,resultIntent);

        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                String color = data.getStringExtra("color");

                switch (color){
                    case "Red":layout = R.drawable.list_shape_red;

                        break;
                    case "Blue":layout = R.drawable.list_shape_blue;
                        break;
                    case"Orange":layout = R.drawable.list_shape_orange;
                        break;
                    case"Green":layout = R.drawable.list_shape_green;
                        break;
                    case"Dark":layout = R.drawable.list_shape_dark;
                        break;
                    default:layout = R.drawable.list_shape;
                }
                setColorBtn.setBackgroundResource(layout);
            }
        }
        if(requestCode == 5){
            if(resultCode == RESULT_OK){
                String userAction = data.getStringExtra("choice");
                if(userAction.equals("YES")){
                    changeItem();
                    finish();
                }
            }
        }

        if(requestCode == 6){
            if(resultCode == RESULT_OK){
                if(data.getStringExtra("choice").equals("YES")){
                    titleText = title.getText().toString();
                    descriptionText = description.getText().toString();
                    resultIntent = new Intent();
                    resultIntent.putExtra("Title", titleText);
                    resultIntent.putExtra("Description",descriptionText);
                    resultIntent.putExtra("changePosition",position);
                    if(DateString != null || DateString != "")
                        resultIntent.putExtra("EstimatedDate",DateString);
                    if(TimeString != null || TimeString != "")
                        resultIntent.putExtra("EstimatedTime",TimeString);
                    resultIntent.putExtra("Category",newCategory);
                    resultIntent.putExtra("layout",layout);
                    resultIntent.putExtra("Completed",true);
                    setResult(RESULT_OK,resultIntent);
                    finish();
                }
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);
        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        DateString = DateFormat.getDateInstance().format(c.getTime());
        dateTextView.setText(DateString);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        int hour;
        String ampm;
        if(hourOfDay >= 13 ){
            ampm = "PM";
            hour = hourOfDay - 12;
        }else{
            ampm = "AM";
            hour = hourOfDay;
        }


        TimeString = hour + ":" + minute + " " + ampm;
        timeTextView.setText(TimeString);
    }

    public void LoadCategories(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("CATEGORIES",null);
        Type type = new TypeToken<ArrayList<String>>(){}.getType();
        categories = gson.fromJson(json,type);

        if(categories == null){
            categories = new ArrayList<>();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        newCategory = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
