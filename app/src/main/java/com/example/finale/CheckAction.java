package com.example.finale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

public class CheckAction extends Activity {
    Button yes,no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_action_layout);
        yes = (Button)findViewById(R.id.actionYESBtn);
        no = (Button)findViewById(R.id.actionNOBtn);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("choice","YES");
                setResult(RESULT_OK,i);
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("choice","NO");
                setResult(RESULT_CANCELED,i);
                finish();
            }
        });
    }
}
