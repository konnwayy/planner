package com.example.finale;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCategory extends AppCompatActivity {

    Button addCategory,cancel;
    EditText category;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_layout);

        addCategory = (Button)findViewById(R.id.addCategoryBtn);
        cancel = (Button)findViewById(R.id.cancelAddCategoryBtn);
        category = (EditText)findViewById(R.id.categoryNameET);

        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                String newCategory;
                newCategory = category.getText().toString();
                if(newCategory.length() > 0){
                    i.putExtra("CATEGORY",newCategory);
                    setResult(RESULT_OK,i);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"New Category Cannot be Blank",Toast.LENGTH_LONG).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                setResult(RESULT_CANCELED,i);
                finish();
            }
        });
    }
}
