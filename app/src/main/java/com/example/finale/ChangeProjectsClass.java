package com.example.finale;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ChangeProjectsClass extends AppCompatActivity {

    EditText Title,Description,Step,MaxBudget;
    TextView ToolbarTitle;
    Button addStep,addProjectBtn,backArrow;
    FrameLayout stepsView;
    boolean changeStep = false;
    int selectedPos,ProjectPosition;
    ArrayList<String> Steps = new ArrayList<>();

    private RecyclerView RecyclerView;
    private  StepAdapter Adapter;
    private RecyclerView.LayoutManager LayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.projects_add_modify_item_layout);

        stepsView = (FrameLayout)findViewById(R.id.stepsLayout);
        ToolbarTitle = (TextView)findViewById(R.id.toolbarChangeTitle);
        ToolbarTitle.setText("Your Project");
        Title = (EditText)findViewById(R.id.projectTitle);
        Description = (EditText)findViewById(R.id.projectNotes);
        Step = (EditText)findViewById(R.id.ProjectStep);
        MaxBudget = (EditText)findViewById(R.id.projectBudget);
        addStep = (Button)findViewById(R.id.addStepProject);
        addProjectBtn = (Button)findViewById(R.id.toolbarAddProjectBtn);
        backArrow = (Button)findViewById(R.id.backArrow);

        Bundle args = this.getIntent().getExtras();
        Title.setText(args.getString("TITLE"));
        Description.setText(args.getString("NOTES"));
        MaxBudget.setText(args.getString("BUDGET"));
        Steps = args.getStringArrayList("STEPS");
        ProjectPosition = args.getInt("POSITION");

        if(Steps.size() <= 0)
            stepsView.setVisibility(View.GONE);
        else
            stepsView.setVisibility(View.VISIBLE);

        RecyclerView = (RecyclerView)findViewById(R.id.ProjectsRecyclerView);
        RecyclerView.setHasFixedSize(true);
        LayoutManager = new LinearLayoutManager(getApplicationContext());
        Adapter = new StepAdapter(Steps);
        RecyclerView.setLayoutManager(LayoutManager);
        RecyclerView.setAdapter(Adapter);
        Adapter.setOnItemClickListener(new StepAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Step.setText(Steps.get(position));
                addStep.setText("OK");
                changeStep = true;
                selectedPos = position;
            }
        });

        addProjectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent result = new Intent();
                result.putExtra("TITLE",Title.getText());
                result.putExtra("DESCRIPTION",Description.getText());
                result.putExtra("STEPS",Steps);
                result.putExtra("BUDGET",MaxBudget.getText());
                result.putExtra("POSITION",ProjectPosition);
                setResult(2,result);
                finish();
            }
        });

        addStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Step.length() > 0 && changeStep == false){
                    Steps.add(Step.getText().toString());
                    Step.setText("");
                    Adapter.notifyItemChanged(0);
                    stepsView.setVisibility(View.VISIBLE);
                }else if(changeStep == true){
                    addStep.setText("ADD STEP");
                    Steps.set(selectedPos,Step.getText().toString());
                    Adapter.notifyItemChanged(selectedPos);
                    changeStep = false;
                }
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });


    }
}
