package com.example.finale;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.TestLooperManager;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.security.PublicKey;
import java.text.DateFormat;
import java.util.ArrayList;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Collections;


import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class Tasks extends Fragment {
    private RecyclerView mRecyclerView;
    private  RecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public View v;



    ArrayList<ListItems> Goals = new ArrayList<>();
    ArrayList<ListItems> CompletedItems = new ArrayList<>();


    ListItems temp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.list_layout_items, container, false);
        loadData();

        mRecyclerView = (RecyclerView)v.findViewById(R.id.recyclerView1);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new RecyclerAdapter(Goals);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                changeItem(position);
            }
        });


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder dragged, RecyclerView.ViewHolder target) {
                int positionDragged = dragged.getAdapterPosition();
                int positionTarget = target.getAdapterPosition();

                Collections.swap(Goals,(Goals.size()-1) - positionDragged,(Goals.size()-1) - positionTarget);
                mAdapter.notifyItemMoved(positionDragged,positionTarget);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                temp = Goals.get((Goals.size() - 1) - viewHolder.getAdapterPosition());
                Goals.remove((Goals.size() - 1) - viewHolder.getAdapterPosition());
                saveData();
                mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                Toast.makeText(getContext(), "Task Deleted", Toast.LENGTH_SHORT).show();

                Snackbar sb = Snackbar.make(getView(),"Are you Sure you Want to Delete",15000)
                        .setAction("UNDO", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Goals.add(temp);

                                mAdapter.notifyItemInserted(0);
                                mRecyclerView.scrollToPosition(View.SCROLL_INDICATOR_TOP);
                                mLayoutManager.scrollToPosition(0);
                                saveData();
                            }
                        });
                sb.show();
            }
        }).attachToRecyclerView(mRecyclerView);

        return v;
    }

    public void insertItem(String title, String description,String dateCreated,String EstimatedDate,String EstimatedTime, int layout,String Category) {
        Goals.add(Goals.size(), new ListItems(title,description,dateCreated,EstimatedDate,EstimatedTime,layout,Category));
        mAdapter.notifyItemInserted(0);
        mRecyclerView.scrollToPosition(View.SCROLL_INDICATOR_TOP);
        mLayoutManager.scrollToPosition(0);
    }

//***** change selected item *********

    public void changeItem(int position) {
        int newPosition = (Goals.size() - 1) - position;
        String title = Goals.get(newPosition).getTitle();
        String description = Goals.get(newPosition).getDescription();
        String estimatedDate = Goals.get(newPosition).getEstimatedFinishDate();
        String estimatedTime = Goals.get(newPosition).getEstimatedFinishTime();
        String currentCategory = Goals.get(newPosition).getCategory();
        int layout = Goals.get(newPosition).getLayout();
        Intent i = new Intent(getActivity(),ChangeItemClass.class);
        i.putExtra("goalPosition",position);
        i.putExtra("title",title);
        i.putExtra("description",description);
        i.putExtra("estimatedDate",estimatedDate);
        i.putExtra("estimatedTime",estimatedTime);
        i.putExtra("layout",layout);
        i.putExtra("category",currentCategory);
        startActivityForResult(i,2);
    }

    public void itemChangedSelected(int position,String title,String description,String endDate,String endTime,int layout,String newCategory){
        Goals.get((Goals.size() - 1) - position).setTitle(title);
        Goals.get((Goals.size() - 1) - position).setDescription(description);
        Goals.get((Goals.size() - 1) - position).setEstimatedFinishDate(endDate);
        Goals.get((Goals.size() - 1) - position).setEstimatedFinishTime(endTime);
        Goals.get((Goals.size() - 1) - position).setLayout(layout);
        Goals.get((Goals.size() - 1) - position).setCategory(newCategory);
        mAdapter.notifyItemChanged(position);
        saveData();
    }

    public void itemCompleted(int position){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("completed",null);
        Type type = new TypeToken<ArrayList<ListItems>>(){}.getType();
        CompletedItems = gson.fromJson(json,type);
        if(CompletedItems == null){
            CompletedItems = new ArrayList<>();
        }

        CompletedItems.add(Goals.get((Goals.size() - 1) - position));
        Goals.remove((Goals.size() - 1) - position);
        mAdapter.notifyItemRemoved(position);
        saveData();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson save = new Gson();
        String saveJson = save.toJson(CompletedItems);
        editor.putString("completed",saveJson);
        editor.apply();
    }



    public void openDialog() {//opens the add item class
        Intent i = new Intent(getActivity(),AddItemClass.class);
        startActivityForResult(i,1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {//item is added
            if (resultCode == RESULT_OK) {
                Calendar c = Calendar.getInstance();
                String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
                String dateCreated = "Created on - " + currentDate;
                String title = data.getStringExtra("Title");
                String description = data.getStringExtra("Description");
                String endDate = data.getStringExtra("EstimatedDate");
                String endTime = data.getStringExtra("EstimatedTime");
                int layout = data.getIntExtra("layout",0);

                String Category = data.getStringExtra("Category");
                insertItem(title,description,dateCreated,endDate,endTime,layout,Category);
                Toast.makeText(getContext(),"New Item Added",Toast.LENGTH_LONG).show();
                saveData();
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }
        if(requestCode == 2){//item was clicked and is being changed
            if(data != null){
                if(resultCode == RESULT_OK){
                    boolean completed = data.getBooleanExtra("Completed",false);

                    String title = data.getStringExtra("Title");
                    String description = data.getStringExtra("Description");
                    int itemPosition = data.getIntExtra("changePosition",0);
                    String endDate = data.getStringExtra("EstimatedDate");
                    String endTime = data.getStringExtra("EstimatedTime");
                    int layout = data.getIntExtra("layout",0);
                    String changedCategory = data.getStringExtra("Category");

                    if(!completed){
                        itemChangedSelected(itemPosition,title,description,endDate,endTime,layout,changedCategory);
                        Toast.makeText(getActivity(),"Update Successful",Toast.LENGTH_SHORT).show();
                    }else{
                        itemCompleted(itemPosition);
                        Toast.makeText(getActivity(),"Moved to Completed",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public void saveData(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("frag1",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(Goals);
        editor.putString("task list",json);
        editor.apply();
    }


    public void loadData(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("frag1",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("task list",null);
        String categoriesJson = sharedPreferences.getString("CATEGORIES",null);
        Type type = new TypeToken<ArrayList<ListItems>>(){}.getType();
        Type categories = new TypeToken<ArrayList<String>>(){}.getType();
        Goals = gson.fromJson(json,type);
        categories = gson.fromJson(categoriesJson,categories);
        if(Goals == null){
            Goals = new ArrayList<>();
        }
    }


}
