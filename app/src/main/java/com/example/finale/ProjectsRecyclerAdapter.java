package com.example.finale;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ProjectsRecyclerAdapter extends RecyclerView.Adapter<ProjectsRecyclerAdapter.ViewHolder>{
    public ArrayList<ProjectItemsClass> mList;
    private OnItemClickListener mListener;


    public interface OnItemClickListener {
        void onItemClick(int position);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView Title;
        public TextView Notes;
        public TextView DateStarted;
        public TextView EstimatedFinish;
        public TextView MiddleLine;
        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            Title = itemView.findViewById(R.id.ProjectsTitle);
            Notes = itemView.findViewById(R.id.ProjectsNotes);
            DateStarted = itemView.findViewById(R.id.ProjectsDateStarted);
            EstimatedFinish = itemView.findViewById(R.id.ProjectsEstimatedFinish);
            MiddleLine = itemView.findViewById(R.id.middleLine);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }



    public ProjectsRecyclerAdapter(ArrayList<ProjectItemsClass> List){
        mList = List;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {//sets layout of each item to projects_item_layout
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.projects_item_layout,viewGroup,false);
        ViewHolder vh =  new ViewHolder(v,mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ProjectItemsClass currentItem = mList.get((getItemCount() - 1) - i);
        viewHolder.Title.setText(currentItem.getProjectTitle());
        if(currentItem.getNotes().equals(null) || currentItem.getNotes().equals("")) {
            viewHolder.Notes.setVisibility(View.GONE);
            viewHolder.MiddleLine.setVisibility(View.GONE);
        }
        else
            viewHolder.Notes.setText(currentItem.getNotes());
        viewHolder.DateStarted.setText(currentItem.getProjectStartDate());


        if(currentItem.getProjectEstimatedFinish() == null || currentItem.getProjectEstimatedFinish() == "")
            viewHolder.EstimatedFinish.setVisibility(View.GONE);
        else
            viewHolder.EstimatedFinish.setText(currentItem.getProjectEstimatedFinish());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


}


